## Supervised Learning

![[Pasted image 20231015121406.png]]

## Training Data

The training data comes in input pairs $(x,y)$ with $x \in \mathbb{R}^D$ and $y \in \mathcal{C}$.
The entire training set is denoted as: $$\mathcal{D} = \{(x_i,y_i)_{i=1}^N \subseteq \mathbb{R}^D \times \mathcal{C}\}$$With:
- $\mathbb{R}^D$ - $D$ dimensional feature space
- $\mathcal{C}$ - label space
- $x_i$ - input vector of the $i$-th training sample
- $y_i$ - label of the $i$-th training sample
- $N$ - number of training samples

The training set points  $(x_i,y_i)$ drawn from an unknown probability distribution $\mathcal{P}(X,Y)$. 
The goal of supervised learning is to use $\mathcal{D}$ to learn a function $h$ sych that for unseen points $(x,y)\sim \mathcal{P}$: $$h(x) \approx y$$ with high probability.
## The output space $\mathcal{C}$

The output or label space $\mathcal{C}$ can take different forms. Depending on this, we use a specific term to refer to the supervised learning task.

In the case of binary classification: $\mathcal{C} = \{0,1\}$ or $\mathcal{C} = \{-1,+1\}$ 
In the case of multi-class classification: $\mathcal{C}={1,2,...,K}$ with $K \geq 2$
In the case of regression: $\mathcal{C}=\mathbb{R}^O$

## The feature space

The feature vector $x_i$ is a $D$-dimensional vector containing $D$ attributes (or features)
describing the $i$-th sample.
Often $x$ is referred to as input, feature vector, attributes,....

The feature vector can be sparse or dense. If the number of non-zero coordinates in $x \gg D$ then we call it dense, otherwise we call it sparse.

![[Pasted image 20231015123144.png]]

## The hypothesis class

The goal of supervised learning is to use $D$ to learn a function $h: \mathbb{R}^D \rightarrow \mathcal{C}$ that can predict $y$ from $x$. This function $h$ is part of a set called hypothesis class $\mathcal{H}$, so $h \in \mathcal{H}$.

### No free lunch theorem

Choosing the right hypothesis class $\mathcal{H}$ is difficult. Every ML algorithm has to make some assumptions about the data, and the choice of the function will depend on the data.
$\mathcal{H}$ encodes assumptions about the data and its distribution.
**No Free Lunch** : There is no single perfect choice for all problems

## The loss function

First we pick a hypothesis class $\mathcal{H}$, i.e. pick a type of machine learning algorithm.
Next we need to pick the best function withing the hypothesis class $h \in \mathcal{H}$.
Finding the best  $h \in \mathcal{H}$ using $\mathcal{D}$ is denoted the learning process.

![[Pasted image 20231015140836.png]]

The idea is to pick $h \in \mathcal{H}$ making the least mistakes in $\mathcal{D}$ and preferably the simplest.
We will need to define a loss (or risk) function $l: \mathbb{R} \rightarrow \mathbb{R}$ that quantifies how well $h(x)$ approximates $y$.
- The lower the value of $l(y, h(x))$ the better the approximation
- $l(y,y)=0$
- Typically (but now always) $l(y,h(x)) \ge 0$ for all $y, h(x)$

![[Pasted image 20231015141308.png]]

Using the training data $\mathcal{D}$ we can compute the average loss over all the data points:
$$\mathcal{L} = \frac{1}{N} \sum_{i=1}^N l(y_i, h(x_i))$$
Finding the best hypothesis means finding the $h$ that minimizes the loss. This can be formalized as: 
$$h^* =  \mathop{\arg \min}\limits_{h \in \mathcal{H}} \frac{1}{N} \sum_{i=1}^N l(y_i, h(x_i))$$
## Generalization

The goal is to find $h$ such that, for an unseen points $(x,y) \sim \mathcal{P} , h(x) \approx y$. In other words we want $h$ to generalize. When $h(\cdot)$  as a very low loss, but it does not perform well in unseen data, we say there is overfitting causing that our model does not generalize well.
However, the loss over the training set does not give us information about the generalization capabilities of the trained model.

$$\epsilon = \mathbb{E}_{(x,y)\sim\mathcal{P}}[l(y,h(x))]$$
We can resort to data splitting to obtain an estimate of the generalization loss.

## Train/Test Splits

We split $\mathcal{D}$ into three sets:
- Training set $\mathcal{D}_{TR}$, used to learn $h$
- Validation set $\mathcal{D}_{VAL}$, to check for overfitting
- Test set $\mathcal{D}_{TEST}$, to evaluate the chosen $h$ and have an estimate of the generalization error or loss

Typical splits are: 70/10/20, 80/10/10, 60/20/20.
If the samples are drawn i.i.d. from the same distribution $\mathcal{P}$, then the testing loss is an unbiased estimator of the true generalization loss.

It is important to split the data properly to simulate a real life scenario and to avoid data
leakage.
To split data we can use many methods, two of them are:
- Uniformly at random if the data is independent identically distributed
- By time: if the data is collected temporally, the split needs to be done in time

![[Pasted image 20231015142937.png]]

## The learning algorithm

Given an hypothesis class $\mathcal{H}$:
1. Train a model by minimizing the training loss 

$$h^* =  \mathop{\arg \min}\limits_{h \in \mathcal{H}} \frac{1}{\lvert \mathcal{D}_{TR} \rvert} \sum_{(x,y)\in \mathcal{D}_{TR}}^N l(y, h(x))$$

2.  Evaluate the testing loss of the model 

$$\epsilon_{TEST}  =  \frac{1}{\lvert \mathcal{D}_{TEST} \rvert} \sum_{(x,y)\in \mathcal{D}_{TEST}} l(y, h(x))$$

As $\lvert \mathcal{D}_{TEST} \rvert \rightarrow \infty$, $\epsilon_{TEST} \rightarrow \epsilon$.  In words: as we increase the data sample we use to train and test our model, the error of our model becomes closer and closer to the true error.

