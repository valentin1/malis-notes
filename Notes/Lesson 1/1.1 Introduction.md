### Traditional Computer Science vs Machine Learning
![[Pasted image 20231015111013.png]]

Machine Learning is a class of algorithms that improve on a task with experience.

Definition: A computer program is said to learn from experience $E$ with respect to some class of tasks $T$ and performance measure $P$ if its performance at tasks in $T$ , as measured by $P$, improves with experience $E$

