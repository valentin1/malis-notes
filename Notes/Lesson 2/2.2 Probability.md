## Probability Density Function and Probability Mass Function

The probability density function (PDF), or density of a continuous random variable, is a function that describes the relative likelihood for this random variable to take on a given value.  It is the primary means of defining a probability distribution.

A probability mass function (PMF) is a function that gives the probability that a
discrete random variable is exactly equal to some value. 
It is often the primary means of defining a discrete probability distribution.

## Gaussian or Normal Distribution

The Gaussian is the most widely used distribution for continuous variables.
For univariate variables $\mathrm{z}$ it is governed by two parameters: the mean $\mu$ and the variance $\sigma^2$.
For $D$-dimensional vectors, it is governed by $\mu$ and a $D\times D$ covariance matrix $\Sigma$  (symmetric and positive-definite).

### Univariate

$$N(\mathrm{z} \mid \mu, \sigma^2) = \frac{1}{\sigma \sqrt{2 \pi}} \exp \left\{-\frac{\left(z-\mu\right)^2}{2 \sigma^2}\right\}$$

$\mathbb{E}[\mathrm{z}] = \mu$
$\operatorname{var}[\mathrm{z}] = \sigma^2$
### Multivariate

$$
p(\mathbf{z} \mid \mu, \Sigma)=\frac{1}{(2 \pi)^{D / 2}|\mathbf{\Sigma}|^{1 / 2}} \exp \left\{-\frac{1}{2}(\mathbf{z}-\mu)^T \mathbf{\Sigma}^{-1}(\mathbf{z}-\mu)\right\}
$$

$\mathbb{E}[\mathrm{z}] = \mu$
$\operatorname{var}[\mathrm{z}] = \Sigma$

## Bernulli Distribution

Distribution for a single binary variable $\mathrm{z} \in \{0,1\}$.  Used for representing a coin toss.  It is governed by a single continuous parameter $p\in[0,1]$ that represents the probability of $\mathrm{z}=1$.

$$\operatorname{Bern}(\mathrm{z} \mid p) = p^\mathrm{z}(1-p)^{1-\mathrm{z}}$$

$\mathbb{E}[\mathrm{z}] = p$
$\operatorname{var}[\mathrm{z}] = p(1-p)$

## Binomial Distribution

Gives the probability of observing $m$ occurrences of $\mathrm{z}=1$ in a set of $N$ samples from a Bernoulli distribution, where the probability of observing $\mathrm{z}=1$ is $\mathrm{z} \in \{0,1\}$.
 
$$\operatorname{Bin}(m \mid N,p) = {N\choose m} p^m(1-p)^{1-m}$$
The multinomial distribution is a generalization of the binomial distribution.

$\mathbb{E}[\mathrm{z}] = Np$
$\operatorname{var}[\mathrm{z}] = Np(1-p)$

## Multinomial Distribution

Multivariate generalization of the binomial that gives the distribution over counts $m_k$ for a $K$-state discrete variable $\mathrm{z}$ to be in state $k$ given a total number of observations $N$.

$$\mathrm{z} = [\mathrm{z}_1, \mathrm{z}_2, \cdots, \mathrm{z}_k] \mathrm{z}_k \in [0,1]$$

When $K=2$ and $N=1$, it is Bernoulli distribution. When $K=2$ and $N \gt 1$ it is the binomial distribution.  


![[./Images/Pasted image 20231022113300.png]]

With $\mu  = (p_1,\cdots,p_k)^\mathrm{T}$.

## MLE and the Coin Toss Problem (revisited)

You ask yourself, ”What is the probability that this coin comes up heads when I toss it?". You toss it $n=10$ tmes and obtain the following sequence of outcomes:
$$\mathcal{D} = \{H, T , T , H, H, H, T , T , T , T \}$$
Based on these samples, how would you estimate $P(H)$.
This problem can be solved though MLE in two different ways.

### Approach 1: Bernulli trials

Let's define $z\in\{0,1\}$ a random binary variable representing the outcome of a single coin toss, where $z=1$ indicates a heads, $z=0$ indicates a tails.
Thus we can formalize $\mathcal{D} = \{H, T , T , H, H, H, T , T , T , T \}$ as the set $\mathcal{D} = \{z_1, \cdots , z_N \}$ of observed values of $z$.

We just saw that the outcome of a single coin toss follow the Bernoulli distribution.
Parameters:
- $0\le p \le 1$ - Probability of heads
- $q=1-p$ probability of tails

We can use MLE to find $p$.

With all these elements we can now construct the likelihood function, under the assumption that the observations (the coin tosses) are independent, so that:
$$p(\mathcal{D} \mid \theta) = \prod_{i=1}^N p(z_i \mid \theta)$$

The log-likelihood is given by [[2.1 Linear Models for Regression#Maximum Likelihood Estimation (MLE)]]: 

$$
\begin{aligned}
\log p(\mathcal{D} \mid p) & =\sum_{i=1}^N \log p\left(z_i \mid \theta\right) \\
& =\sum_{i=1}^N \log \left(p^{z_i}(1-p)^{1-z_i}\right) \\
& =\sum_{i=1}^N z_i \log p+\left(1-z_i\right) \log (1-p)
\end{aligned}
$$

Using the expression found for $\log p(\mathcal{D} \mid p)$ we can find $p_{MLE}$ by:
$$p_{MLE} = \mathop{\arg \min}\limits_p \sum_{i=1}^N z_i \log p+\left(1-z_i\right) \log (1-p) $$
Next we need to calculate the first derivative:
$$\frac{\partial}{\partial p}\left(\sum_{i=1}^N z_i \log p+\left(1-z_i\right) \log (1-p) \right) =0$$
and solve for p. As an exercise try to do this.


### Approach 1: Binomial distribution

We know that the binomial distribution is nothing else than repeated Bernoulli trials. Parameters:
- $N$: The number of trials, in our example they are $N= n_H + n_T$  
- $p$: The probability of heads

We express the observations as $\mathcal{D} \sim \operatorname{Bin}(N,p)$. Again our goal is to find $p$ with MLE.
$$p(\mathcal{D} \mid \theta) = {{n_H+n_T} \choose n_H} \theta^{n_H} (1-\theta)^{n_T}$$

with $\theta=p$. When we plug-in this expression into our likelihood estimator the product (or sum) disappears. Why?
This leads us to:
$$p_{MLE} = \mathop{\arg \min}\limits_p \log \left({{n_H+n_T} \choose n_H} p^{n_H} (1-p)^{n_T}\right) $$

which is solved through:
$$\frac{\partial}{\partial p}\left(n_H\log p+ n_T \log (1-p) \right) =0$$
We remove the binomial coefficient because it does not depend on p!
Verify this expression leads to the same result as before.


## Joint and Conditional Probabilities

### Joint probabilities

The supervised learning problem has an input X and the corresponding target output vector $y$ with the goal to predict $y$ given a new value $x$.
The joint probability distribution $p(x, y )$ provides a view on the uncertainty of these variables.
Joint probability: For two discrete random variables, $X$ and $Y$ , $P(X = x, Y = y )$ is the probability that random variable $X$ has value $x$ and random $Y$ has value $y$. Meaning that the two events happen at the same time, for example it's Monday and it rains.
Joint density function: For two continuous random variables, $x$ and $y$ , $p(x, y )$ is the joint density function (pdf). Remember that the PDF returns a likelihood.

### Conditional probabilities
When variables are dependent it is possible to work with conditioning. For example: Probability of breaking the world marathon record (B=1) given that the temperature will be above 30 (A=1).

$$P(B = 1|A = 1)$$

![[./Images/Pasted image 20231024101919.png]]


- $P(X,Y )$: Joint probability.
- $P(Y \mid X)$: Conditional probability, e.g. the probability of $Y$ given $X$.
- $P(X)$: Marginal probability, e.g. the probability of $X$.

Illustration:

![[./Images/Pasted image 20231024103634.png]]

In the bottom left we ignore $Y=2$.

### Bayes Theorem

Using the product rule and the symmetry property $P(X,Y) = P(Y,X)$ we obtain the following relationship among conditional probabilities:

$$P(Y \mid X) = \frac{P(X \mid Y)P(Y)}{P(X)}$$
Using the sum rule, the denominator in Bayes’ theorem can be expressed in terms of the quantities appearing in the numerator:

$$P(Y \mid X) = \frac{P(X \mid Y)P(Y)}{\sum_Y P(X \mid Y) P(Y)}$$

![[Pasted image 20231024104743.png]]

### Intuition

Problem statement:
- You are a nurse screening a set of students for a sickness called Diseasitis. 
- We know from past studies that ∼20% of the students get Diseasitis at this time of year 
- Diseasitis is tested using a color-changing tongue depressor. It turns black if the student has Diseasitis. 
- Among patients with Diseasitis, 90% turn the tongue depressor black.
- However, it also turns black 30% of the time for healthy students. 
- You are tested and the tongue depressor gets black. 
- Question: What is your probability of having Diseasitis?

In our problem they become:

![[Pasted image 20231024104831.png]]

- $P(S) = 0.2, P(not\, S) = 0.8$ 
- $P(B \mid S) = 0.9$
- $P(B \mid not\, S)=0.3$
- $P(S \mid B) = \frac{P(B \mid S)P(S)}{P(B)} = \frac{P(B \mid S)P(S)}{P(B \mid S) P(S) +P(B \mid not\, S) P(not \,S)} = \frac{0.9 * 0.2}{(0.9*0.2)+(0.3*0.8)} = 0.43$ 

We say that we don't have COVID because the probability is less that the probability of being not sick
Our intuition tells us that we would choose the class having the higher posterior probability to minimize the chance of assigning x to the wrong class.

## Statistical properties
### Expectations & Covariances

Now, let's have a quick recap on some important statistical properties

**Expectation**: The average of a large number of independent realizations of a random variable
- Discrete: $\mathbb{E}[X] = \sum_X XP(X)$
- Continuous: $\mathbb{E}[X] = \int_X XP(X) \,dx$ 

In either case, if have a finite number $N$ of points drawn from the probability distribution or probability density, then the expectation can be approximated as:

$$\mathbb{E}[X] = \frac{1}{N}\sum_{i=1}^N x_i$$
Conditional Expectation: Expected value of y given x:

$$\mathbb{E}[y\mid x] = \int_y yp(y\mid x) \, dy$$
Covariance: A measure of joint variability of two variables

$$\operatorname{cov}(X,Y)= \mathbb{E}[(X-\mathbb{E}[Y])(Y-\mathbb{E}[X])] = \mathbb{E}[XY] - \mathbb{E}[X]\mathbb{E}[Y]$$

During the last lecture, we saw how to predict a new point once the model parameters $(\hat{\mathbf{w}},\sigma^2)$  were estimated
$$\hat{y}_{\text {new }}=\mathbb{E}\left[y_{\text {new }} \mid \mathbf{x}^* ; \mathbf{w}, \sigma^2\right]=\mathbf{x}^* \hat{\mathbf{w}}$$
As an exercise study the statistical properties of the linear regression estimate: $\hat{\mathbf{w}}$, $\sigma : \mathbb{E}[\hat{\mathbf{w}}], \mathbb{E}[\sigma], \operatorname{cov}(\hat{\mathbf{w}})$.
