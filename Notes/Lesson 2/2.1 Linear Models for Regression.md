
We want to be able to find a curve that fits our data points as best as possible, and to do so we look for the function underlying the points. We start with a guessed function and improve it by trying to minimize the error.

We have $y \in \mathcal{C}$ and $x \in \mathbb{R}^D$.
They are relate by an unknown function $f: \mathbb{R}^D \rightarrow \mathcal{C}$.

Our goal is to predict $y$ using $x$ but we don't know the true relationship, $f$ , between $y$ and $x$. Notice how we changed and started speaking about $f$ while before we were speaking about $h$. This change is due to the fact that $f$ is the ground truth while $h$ is just an approximation of $f$.

To discover the relationship between $x$ and $y$ we have access to data. It consists of a set of $N$ inputs: $x_i = \{1,...,N\}$ and a corresponding set of outputs ${y_i}$. Paired these two form what is known as the training set $\mathcal{D} = \{(x_i,y_i)\}$.
The goal of supervised learning is to use $\mathcal{D}$ to learn a function $h: \mathbb{R}^D \rightarrow \mathcal{C}$, $h \in \mathcal{H}$ that can predict $y$ from $x$.
The first hypothesis class we studied wan nearest neighbors.

## No free lunch

We assume $y \in \mathbb{R}$, on the model side we assume $y = f(x)$ and that $y_i = w^T x_i$. In other words that there is a linear relationship with respect to the parameters of the model.
For example $y_i = \log(x_1^{w_1}e^{w^2}) = w_1w_2\log(x_1e)$. Notice how even in this expression with the logarithm the parameters of the model $w$ are still linear, and while a non-linear function is applied to the features of the data we can still use the linear model for regression.

### Example

![[./Images/Pasted image 20231015151514.png]]

- $y$ is the time
- $x$ is the year
- $D$ is 1 because we have one dataset
- $N$ is the number of datapoints so 27
- $(x_3, y_3) = (1904,11)$
- $\hat{y}$ is the prediction

Now it's time to make assumptions about our data. From the plot we can clearly see that the time is decreasing over the years, and of course the time is always positive, formally:
- $y \in \mathbb{R}$ and $y \gt 0$ 
- $f$ is a decreasing function: $f(x_i) \ge f(x_{i+1})$
- $x,y$ have a linear relationship

Of course our data does not take into consideration the weather, the shoes tech, the wind,... so our prediction will be of limited accuracy!

### Errors

A more accurate model assumption should account for observation errors (or noise).
This is the additive error model: $y = h(x) = f(x) + \epsilon$. 
Error Assumptions:
- The error can be positive or negative.
- It is independent for each $x_i$ 
	- It may be different for every input sample $x_i$
	- Holds no relationship between errors along $X$
- It cannot be modeled exactly. Because if we could be would add it to $y$, but it can be modeled as a continuous random variable

A random variable is a measurable function $X : \Omega \rightarrow E$ from a set of possible outcomes 
$\Omega$ to a measurable space $E$. In other words it is a variable whose value is determined by chance, and we can't tell it's value with 100% accuracy, unlike a deterministic variable.

## Probability Density Function

The probability density function (PDF), or density of a continuous random variable, or probability distribution function, is a function that describes the relative likelihood for this random variable to take on a given value.
The probability of the random variable to fall within a particular region is given by the integral of this variable's PDF over the region:

$$P(a\le X \le b)= \int_a^b p(X) \,dX$$

$p(X)$ can be any probability density function, for example the Gaussian (normal) distribution. To calculate the probability $P(1 \le X \le 2)$ we will have to compute the integral.
But for example the likelihood of $X=3$ is just the height of the normal distribution at that point. Note that the height of the distribution can be any value, also more than 1, but never less than zero. Likelihood is not probability, they are different things.

In a more precise sense, the PDF is used to specify the probability of the random variable falling within a particular range of values, as opposed to taking on any one value. This probability is given by the integral of this variable's PDF over that range, that is, it is given by the area under the density function but above the horizontal axis and between the lowest and greatest values of the range. The probability density function is non-negative everywhere, and the area under the entire curve is equal to 1.

![[./Images/Pasted image 20231015155059.png]]


Its general form is: 
$$p(\mathbf{z}) = \frac{1}{\sigma \sqrt{2\pi}} e^{-\frac{(z-\mu)^2}{2\sigma^2}}$$

Where: 
- $z$ is a continuous random variable
- $\mu$ is the mean of $z$. It controls the height of the bell
- $\sigma^2$ is its variance. It controls the width of the bell

It is often referred to as $\mathcal{N}(\mu, \sigma^2)$.

### Multivariate Gaussian Distribution

Let us denote $z=(z_1,...,z_D)^T$ the multivariate Gaussian or joint normal distribution of $z$ is denoted by:

$$
p(\mathbf{z})=\frac{1}{(2 \pi)^{D / 2}|\mathbf{\Sigma}|^{1 / 2}} \exp \left\{-\frac{1}{2}(\mathbf{z}-\mu)^T \mathbf{\Sigma}^{-1}(\mathbf{z}-\mu)\right\}
$$
Where:
- $\mu \in \mathbb{R}^D$ is its mean (of $z$)
- $\Sigma \in \mathbb{R}^{D \times D}$ is the covariance matrix
- $\lvert \cdot \rvert$ is the determinant

It is denoted $\mathcal{N}(\mu, \Sigma)$.

### The covariance matrix

This matrix gives us an idea of the correlation of our data points with respect of their variables. Taken two features of our datapoints we want to know if there is any correlation of these two features.
If there is no correlation between the two features we will just find values on the diagonal, if there are positive values, then there is a positive correlation, otherwise a negative correlation (in the second diagonal).

![[./Images/Pasted image 20231015161646.png]]

## Linear models for regression

We assume: 
- $y_i \in \mathbb{R}$ 
- $y_i = w^T x_i + \epsilon_i$, where $\epsilon_i \sim \mathcal{N}(0,\mu^2)$, so every error has a Gaussian distribution 

This is equivalent to $y_i \mid x_i \sim \mathcal{N}(w^T x_i, \sigma^2)$.

![[./Images/Pasted image 20231015162435.png]]

Remember that the task of learning consists in finding a hypothesis $h: \mathbb{R}^D \rightarrow \mathcal{C}$ that can make a good prediction of $y$ given $x$: $\hat{y}=h(x)$, where $\hat{y}$ denotes the predicted value of $y$.
As always our goal is to $\hat{y} \approx y$ for unseen $(x,y)$ pairs. To achieve this make us of the data and of any prior knowledge we might have about $f$ .

## Meaning of $w$

![[./Images/Pasted image 20231015163510.png]]

The term "linear" comes from the fact that $y$ is linear w.r.t the parameters $w$.
Of course $y = x_0 + x_1^2*w$ is still linear, because we just squared the features not the parameter $w$.

## Learning (Model fitting)

Model fitting is the process of finding a good estimate of $h(x)$. It accounts to fitting the training data $\mathcal{D}$ into the linear model to estimate the model parameters.
$$p(y_i \mid x_i; w, \sigma^2) = \frac{1}{\sigma \sqrt{2 \pi}} \exp \left\{-\frac{\left(y_i-w^T \mathbf{x}_i\right)^2}{2 \sigma^2}\right\}$$

- $w={w_j}, \sigma^2$ are the parameters of the model
- In the $k$-NN we have no parameters! We just had $k$, but that is an hyperparameter, so a parameter that we can set.

For each data point with features $x_i$ , the label $y$ is drawn from a Gaussian with
mean $w^T x_i$ and variance $\sigma^2$. Our task is to estimate the slope $w_1$ and intercept $w_0$ from the data using the fact that $\hat{y} \approx y$.

## Coin flipping

Ask your self:
> "What is the probability that this coin comes up heads when I toss it?"

You toss it $n=10$ times and obtain the following sequence of outcomes: $D=\{H,T,T,H,H,H,T,T,T,T\}$. Based on these samples you would guess: $n_H=4, n_T=6, P(H) \approx \frac{n_H}{n_H + n_T}=\frac{4}{10}=0.4$.
Can we derive this formula more formally?

### A closer look
The estimation process we just performed is nothing else than the Maximum Likelihood
Estimate (MLE). For MLE, you typically proceed in two steps:
1. You make an explicit modeling assumption about what type of distribution your data was sampled from.
2. You set the parameters of this distribution so that the data you observed is as likely as possible.

Coin Toss example: The observed outcomes of a coin toss follow a binomial distribution. It has two parameters $n$ and $\theta$ and it captures the distribution of $n$ independent binary random events that have a positive outcome with probability $\theta$. $n$ is the number of tosses and the probability of having heads $P(H) = \theta$.
We need to find $\hat{\theta}$ given our observed data $D$.

## Maximum Likelihood Estimation (MLE)

Let $Z_1,Z_2,\ldots,Z_N$ be a random sample of iid random variables with joint PDF $p_{\theta}(z)$ that depends on $\theta$.
The joint PDF is:
$$
\begin{aligned}
p_\theta\left(z_1, z_2, \ldots, z_N\right) & =p\left(z_1, z_2, \ldots, z_N \mid \theta\right) \\
& =p\left(z_1 ; \theta\right) p\left(z_2 ; \theta\right) \ldots p\left(z_N ; \theta\right) \\
& =\prod_{i=1}^N p\left(z_i \mid \theta\right)
\end{aligned}
$$

We can break the $p(z_1,z_2,\ldots,z_n \mid \theta)$ into $p(z_1;\theta)p(z_2;\theta)\ldots p(z_n;\theta)$ because they are independent and identically distributed random variables.
Remember that the PDF returns a likelihood, not a probability!

Upon observing the data, $p(z_1;\theta)p(z_2;\theta)\ldots p(z_n;\theta)$, becomes a function of just $\theta$, and so they are no more random because we have access from actual samples of the random distribution.

So we can rewrite it as:
$$ \mathcal{L}(\theta) = \prod_{i=1}^N p(z_i \mid \theta)$$
In words: $\mathcal{L}(\theta)$ is the likelihood of observing the given data given $\theta$. We will denote $\mathcal{L}(\theta)$ as the likelihood function.

**Definition**: The Maximum Likelihood Estimator of $\theta$ (MLE) is the value $\hat{\theta}$ that maximizes the likelihood. It is the value that makes the data the most "probable".

Finding $\hat{\theta}$ that maximizes the likelihood of the data $p(Z\mid \theta)$ accounts to:
$$\hat{\theta}_{MLE} =
\mathop{\arg \max}\limits_{\theta} \prod_{i=1}^N p(z_i \mid \theta)$$
Rather than maximizing this product, which can be complex, we can use the fact that the logarithm is a monotonic function so it will be equivalent to maximize the log likelihood:

$$\ell(\theta) = \sum_{i=1}^N \log(p(z_i \mid \theta))$$
To understand why we do this is just for convenience, because its much easier to then calculate the derivative of a log rather than a product, additionally we just care to find the point of the maximum, the value at the maximum is not important, se we can apply any monotonous transformation such as the logarithm.

So instead of $\prod_{i=1}^{N} p(z_i \mid \theta)$ we do  $\log{(\prod_{i=1}^{N} p(z_i \mid \theta))}$ that then thanks to properties of the logarithm becomes: $\sum_{i=1}^{N} \log(p(z_i \mid \theta))$ (because $\log(ab)=\log(a)+\log(b)$).
Replacing accordingly we get:
$$\hat{\theta}_{MLE} = \mathop{\arg \max}\limits_{w} \sum_{i=1}^N \log(p(z_i \mid \theta))$$
## Estimating $\hat{\theta}$: HOWTO

1. Plug in all the terms for the distribution in Eq. 1
2. Take the log of the function.
3. Compute its derivative, and equate it with zero to find an extreme point.
4. (Optional) To be precise, verify that it is a maximum and not a minimum, by verifying that the second derivative is negative.

## Solving Linear Regression with MLE

### Estimating $\hat{w}$ using MLE

Let us recall our assumption about the distribution of $y$:
$$p(y_i \mid x_i; w, \sigma^2) = \frac{1}{\sigma \sqrt{2 \pi}} \exp \left\{-\frac{\left(y_i-w^T \mathbf{x}_i\right)^2}{2 \sigma^2}\right\}$$
We can use the MLE to estimate $\hat{w}$.
$$
\begin{aligned}
& \hat{\mathbf{w}}=\underset{\mathbf{w}}{\arg \max } \prod_{i=1}^N p\left(y_i \mid \mathbf{x}_i ; \mathbf{w}, \sigma\right) \\
& =\underset{w}{\arg \max } \prod_{i=1}^N \frac{1}{\sigma \sqrt{2 \pi}} \exp \left\{-\frac{\left(y_i-w^T \mathbf{x}_i\right)^2}{2 \sigma^2}\right\} \\
& =\underset{w}{\arg \max } \sum_{i=1}^N \log \left(\frac{1}{\sigma \sqrt{2 \pi}} \exp \left\{-\frac{\left(y_i-\mathbf{w}^T \mathbf{x}_i\right)^2}{2 \sigma^2}\right\}\right) \\
& =\underset{w}{\arg \max } \sum_{i=1}^N \log \left(\frac{1}{\sigma \sqrt{2 \pi}}\right)+\log \left(\exp \left\{-\frac{\left(y_i-w^T \mathbf{x}_i\right)^2}{2 \sigma^2}\right\}\right) \\
& =\underset{w}{\arg \max } \sum_{i=1}^N \log \left(\frac{1}{\sigma \sqrt{2 \pi}}\right)-\frac{\left(y_i-w^T \mathbf{x}_i\right)^2}{2 \sigma^2} \\
& =\underset{w}{\arg \max }-\sum_{i=1}^N \frac{\left(y_i-\mathbf{w}^T \mathbf{x}_i\right)^2}{2 \sigma^2} \\
& =\underset{w}{\arg \min } \frac{1}{2 \sigma^2} \sum_{i=1}^N\left(y_i-\mathbf{w}^T \mathbf{x}_i\right)^2 \\
& =\underset{w}{\arg \min } \frac{1}{2 \sigma^2} \sum_{i=1}^N\left(y_i-w^T x_i\right)^2 \\
& =\underset{w}{\arg \min } \sum_{i=1}^N\left(y_i-w^T \mathbf{x}_i\right)^2 \\
&
\end{aligned}
$$
Notice how: 
1. On the 3rd we exploit the log property
2. On the 7th step we switched from $\mathop{\arg \max}$ to $\mathop{\arg \min}$ because of the minus sign

The last expression: 
$$\hat{w} =
\mathop{\arg \min}\limits_{w} \sum_{i=1}^N(y_i-w^{T}x_i)^2$$

Is the quadratic loss function:
$$
\hat{w} = \mathop{\arg \min }\limits_{w}(\mathcal{L}) = \mathop{\arg \min}\limits_{w} \sum_{i=1}^{N} (y_i - w^T x_i)^2$$
$x$, $y$ and $w$ can have large dimension. Because current notation can be cumbersome to handle we will favor the use of matrix notation:

![[./Images/Pasted image 20231018153034.png]]

Using the matrix notation, the expression we had obtained for $\hat{w}$ becomes:
$$\mathop{\arg \min }\limits_{\mathrm{w}} \frac{1}{N}(y-\mathrm{Xw})^T(y-\mathrm{Xw})$$
Following the MLE HOWTO, now we need to solve for:
$$
\frac{\partial \mathcal{L}}{\partial w} = \frac{\partial}{\partial w} \left( \frac{1}{N}(y-Xw)^T(y-Xw)) \right)$$
### Matrix Derivates Cheat Sheet

All bold capitals are matrices, bold lowercase are vectors.

|Rule No.| Rule | Comment|
|- | -------| ---------|
| 1 | $(\mathbf{AB})^T$ = $\mathbf{B}^T\mathbf{A}^T$| Order is reversed, everything is transposed |
| 2| $(\mathbf{aBc})^T = \mathbf{c}^T\mathbf{B}^T\mathbf{a}$ | Order is reversed, everything is transposed |
| 3| $\mathbf{a}^T\mathbf{b} = \mathbf{b}^T \mathbf{a}$ | The result is a scalar, and the transpose of the scalar itself|
| 4| $(\mathbf{A}+\mathbf{B})\mathbf{C} = \mathbf{AC} + \mathbf{BC}$ | Multiplication is distributive|
| 5| $(\mathbf{a}+\mathbf{b})^T\mathbf{C} = \mathbf{a}^T\mathbf{C} + \mathbf{b}^T\mathbf{C}$ | Multiplication is distributive (with vectors)| 
| 6| $\mathbf{AB} \ne \mathbf{BA}$ | Multiplication is **not** commutative| 

|Rule No.|Vector derivate|
|-| - |
|7| $\mathbf{x}^T\mathbf{B} \rightarrow \mathbf{B}$|
|8| $\mathbf{x}^T\mathbf{b} \rightarrow \mathbf{b}$|
|9| $\mathbf{x}^T\mathbf{x} \rightarrow 2\mathbf{x}$|
|10| $\mathbf{x}^T\mathbf{Bx} \rightarrow 2\mathbf{Bx}$|

Remember that if we multiply a matrix of dimensions $(m,n)$ by a matrix $(n,p)$ we get a matrix of dimensions $(m,p)$. The multiplication can only be performed if the number of columns of the first matrix is equal to the number of rows of the second matrix.

First let's calculate the derivative:
$$
\begin{aligned}
& \frac{\partial}{\partial \mathbf{w}} \left( \frac{1}{N}\left(\mathbf{y}-\mathbf{Xw}\right)^T\left(\mathbf{y}-\mathbf{Xw}\right) \right) = 0 \\

& \frac{1}{N} \frac{\partial}{\partial \mathbf{w}} \left( \left(\mathbf{y}^T(\mathbf{y}-\mathbf{Xw})\right)- (\mathbf{Xw})^T(\mathbf{y}-\mathbf{Xw}) \right) = 0 \\

& \frac{\partial}{\partial \mathbf{w}} \left( \mathbf{yy}^T  - \mathbf{y}^T\mathbf{Xw} - \mathbf{w}^T\mathbf{X}^T\mathbf{y}+\mathbf{w}^T\mathbf{X}^T\mathbf{Xw} \right) = 0 \\
& \frac{\partial}{\partial \mathbf{w}} \left( \mathbf{yy}^T  - \mathbf{w}^T\mathbf{X}^T\mathbf{y} - \mathbf{w}^T\mathbf{X}^T\mathbf{y}+\mathbf{w}^T\mathbf{X}^T\mathbf{Xw} \right) = 0 \\
& \frac{\partial}{\partial \mathbf{w}} \left(  - 2\mathbf{w}^T\mathbf{X}^T\mathbf{y}+\mathbf{w}^T\mathbf{X}^T\mathbf{Xw} \right) = 0 \\
& - 2\mathbf{X}^T\mathbf{y}+2\mathbf{X}^T\mathbf{X}\mathbf{w} = 0 \\
& - 2\mathbf{X}^T\mathbf{y}+2\mathbf{X}^T\mathbf{X}\mathbf{w} = 0\\
& 2\mathbf{X}^T\mathbf{X}\mathbf{w} = 2\mathbf{X}^T\mathbf{y} \\
& \mathbf{X}^T\mathbf{X}\mathbf{w} = \mathbf{X}^T\mathbf{y} \\
& (\mathbf{X}^T\mathbf{X})^{-1}(\mathbf{X}^T\mathbf{X})\mathbf{w} = (\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T\mathbf{y} \\
& \mathbf{w} = (\mathbf{X}^T\mathbf{X})^{-1}\mathbf{X}^T\mathbf{y} \\
& (\mathbf{X}^T\mathbf{X})\mathbf{w} = \mathbf{X}^T\mathbf{y} \\
&
\end{aligned}
$$
Remember that $\mathbf{X}=(N \times D), \mathbf{y}=(N \times 1), \mathbf{W}=(D \times 1)$.
1. On step 1 we apply Rule 5
2. On step 2 we apply Rule 5 again
3. On step 3 we apply Rule 2
4. On step 4 we remove the components that do not depend on $\mathbf{w}$
5. On step 5 we apply rule 7 and 10
6. On step 9 we multiply both sides with $(\mathbf{X}^T\mathbf{X})^{-1}$ to simplify
7. Then we rearrange

So we have that: $$(\mathbf{X}^T\mathbf{X})\mathbf{\hat{w}} = \mathbf{X}^T\mathbf{y}$$
The expression we obtained is commonly know as the ordinary least squares (OLS). We have found a general expression to obtain the unknown parameters of a linear regressor.
We avoid using calculating the $(\mathbf{X}^T\mathbf{X})^{-1}$ because it is computationally expensive.

If features are not linearly independent, or if $N \ll D$, the estimation of $\mathbf{\hat{w}}$ will contain some errors.

## Predictions

Once $\mathrm{\hat{w}}$ has been estimated, the fitted model can be used to predict new values of $\mathrm{\hat{y}}$: 
$$\mathbf{\hat{y}}_{\mathrm{new}} = \mathrm{x}_{\mathrm{new}} \mathrm{\hat{w}}$$
Where $\mathrm{x}_{\mathrm{new}}$ is a set of unseen input data.
The matrix $\mathrm{x}_{\mathrm{new}}$ s constructed in the same way as it was done for the training set, but using $\mathrm{x}^*$.  

In the 100m Olympics problem $\mathrm{x}^*$ would be the year for which we want to predict the finishing time.

## The role of $\sigma^2$

As the distribution of $\mathrm{y}$ has two parameters, $\hat{\mathrm{w}}$ and $\sigma$. 
$$p(y_i \mid x_i; w, \sigma^2) = \frac{1}{\sigma \sqrt{2 \pi}} \exp \left\{-\frac{\left(y_i-w^T \mathbf{x}_i\right)^2}{2 \sigma^2}\right\}$$We can also use the MLE to find an estimation of $\sigma$.

For simplicity, we will use the matrix notation for this derivation:
$$\prod_{i=1}^N p(y_i \mid x_i; w, \sigma^2) = p(\mathbf{y} \mid \mathbf{X}; \mathbf{w}, \Sigma)$$
### Link between $\Sigma$ and $\sigma^2$

We need to find the link between $\Sigma$ and $\sigma$. For this, lets have a look at the distribution of $y$ before plugin it into the MLE:

$$
p(\mathbf{y} | \mathbf{X}; \mathbf{w}, \mathbf{\Sigma})=\frac{1}{(2 \pi)^{D / 2}|\mathbf{\Sigma}|^{1 / 2}} \exp \left\{-\frac{1}{2}(\mathbf{y}-\mathbf{Xw})^T \mathbf{\Sigma}^{-1}(\mathbf{y}-\mathbf{Xw})\right\}
$$
As the noise is independent for every $\mathbf{x}_i$: 

![[./Images/Pasted image 20231018172805.png]]

In other words we assume independence.
Replacing the term for $\mathbf{\Sigma}$ we get:
$$
p(\mathbf{y} | \mathbf{X}; \mathbf{w}, \sigma^2\mathbf{I})=\frac{1}{(2 \pi\sigma)^{N / 2}} \exp \left\{-\frac{1}{2\sigma^2}(\mathbf{y}-\mathbf{Xw})^T (\mathbf{y}-\mathbf{Xw})\right\}
$$
We can apply this into the MLE HOWTO algorithm and get:
$$\begin{aligned} \hat{\sigma}^2 & =\underset{\sigma^2}{\arg \max } \log p\left(\mathbf{y} \mid \mathbf{X} ; \mathbf{w}, \sigma^2\right) \\ & =\underset{\sigma^2}{\arg \max } \log \frac{1}{\left(2 \pi \sigma^2\right)^{N / 2}}+\log \left(\exp \left\{-\frac{1}{2 \sigma^2}(\mathbf{y}-\mathbf{X} \mathbf{w})^T(\mathbf{y}-\mathbf{X} \mathbf{w})\right\}\right) \\ & =\underset{\sigma^2}{\arg \max }-\frac{N}{2} \log 2 \pi \sigma^2+\log \left(\exp \left\{-\frac{1}{2 \sigma^2}(\mathbf{y}-\mathbf{X} \mathbf{w})^T(\mathbf{y}-\mathbf{X} \mathbf{w})\right\}\right) \\ & =\underset{\sigma^2}{\arg \max }-\frac{N}{2} \log (\mathbf{2} \pi)-\frac{N}{2} \log \sigma^2-\frac{1}{2 \sigma^2}(\mathbf{y}-\mathbf{X} \mathbf{w})^T(\mathbf{y}-\mathbf{X} \mathbf{w}) \\ & =\underset{\sigma^2}{\arg \min } \frac{N}{2} \log \sigma^2+\frac{1}{2 \sigma^2}(\mathbf{y}-\mathbf{X} \mathbf{w})^T(\mathbf{y}-\mathbf{X} \mathbf{w})\end{aligned}$$

Then we can calculate its derivative:
$$
\begin{aligned}
& \frac{\partial}{\partial \mathbf{\sigma^2}} \left( \frac{N}{2} \log \sigma^2+\frac{1}{2 \sigma^2}(\mathbf{y}-\mathbf{X} \mathbf{w})^T(\mathbf{y}-\mathbf{X} \mathbf{w}) \right) = 0 \\
& \frac{N}{2\sigma^2} -\frac{1}{2 \sigma^4}(\mathbf{y}-\mathbf{X} \mathbf{w})^T(\mathbf{y}-\mathbf{X} \mathbf{w}) = 0 \\
& \frac{N\sigma^2-(\mathbf{y}-\mathbf{X} \mathbf{w})^T(\mathbf{y}-\mathbf{X} \mathbf{w})}{2\sigma^4}  = 0 \\
& N\sigma^2-(\mathbf{y}-\mathbf{X} \mathbf{w})^T(\mathbf{y}-\mathbf{X} \mathbf{w})  = 0 \\
& N\sigma^2 = (\mathbf{y}-\mathbf{X} \mathbf{w})^T(\mathbf{y}-\mathbf{X} \mathbf{w}) \\
& \sigma^2 = \frac{1}{N}(\mathbf{y}-\mathbf{X} \mathbf{\hat{w}})^T(\mathbf{y}-\mathbf{X} \mathbf{\hat{w}}) \\
&
\end{aligned}
$$
The obtained expression is nothing else than the standard estimate of the variance.

Note that $\hat{w}$ intervenes in the estimation of $\sigma^2$.
What information do we gain by having $\sigma^2$?

## Predictions

New values of $\mathbf{\hat{y}}$ are obtained through:
$$\mathbf{\hat{y}}_{\mathrm{new}} = \mathbb{E} \left[\mathbf{y}_{\mathrm{new}} \mid \mathbf{X}; \mathbf{w}, \sigma^2 \right] = \mathbf{X}_{\mathrm{new}}\mathbf{\hat{w}}$$
What information do we gain by having $\sigma^2$?

$$\mathbf{\hat{y}}_{\mathrm{new}} = \mathcal{N}\left(\mathcal{\hat{w}}^T\mathbf{x}_{\mathrm{new}},\sigma^2 \right) $$
 It has the purpose of adding uncertainty, because it controls the width of the bell curve and also it measures this uncertainty.
 The higher the $\sigma^2$ the higher the uncertainty.
 