
### An old friend

Assume this is how your training data looks like. You have chosen $K = 3$. A new test point arrives. What do you do?

![[./Images/Pasted image 20240103152058.png]]

We have to compare our new point with every other point of the dataset!
Testing time complexity: O(ND). Ideally $N \gg 0$.
But we can improve this!
Imagine the following setup: The orange points represent the training points (no labels). We want to predict $\hat{y}$ for the yellow point.

![[./Images/Pasted image 20240103152432.png]]

Intuition: Partition of the D-dimensional feature space to make the search of the K nearest neighbors faster.

Let's say we have $D=2$.
Training step:
1. Divide the data along one feature by setting a threshold $t_1$
2. Divide again along the remaining feature by setting a threshold $t_2$
3. Record the quadrant each $x$ belongs to

![[./Images/Pasted image 20240103152700.png]]

Test step $K=1$:
1. Find the quadrant Q of $x'$
2. $d_x$: find distance of $x'$ to closest point in Q
3. $d_{t_1}=d(x',t_1)$  
4. $d_{t_2}=d(x',t_2)$
5. Case 1: $d_x \lt d_{t_1} \land d_x \lt d_{t_2}$ 
6. Case 2: $d_x \lt d_{t_1} \land d_x \gt d_{t_2}$ check neighbor quadrant (via $t_2$)
7. Case 3: $d_x \gt d_{t_1} \land d_x \lt d_{t_2}$ check neighbor quadrant (via $t_1$)
8. Case 3: $d_x \gt d_{t_1} \land d_x \gt d_{t_2}$ check all other quadrants. Case 4 is as bad as standard kNN

![[./Images/Pasted image 20240103153510.png]]

The number of elements in a leaf node (leaf size) is a hyper-parameter. Question: What is the leaf size in the example? 3.

### Some open questions

Q: How should the data be split during training?
A: Split the data in half recursively by rotating through the features. When rotating, choosing the feature with maximum variance is a good heuristic

Q: How to define the threshold for the split?
A: Use the median over the range of values of the feature to be split

Q: How to extend the algorithm to $K \gt 1$?
A: Instead of considering the distance to the closest point, take $K$ closest distances

Q: How often we may fall into a situation like case 4?
A: Let's answer to this question by recalling Lab 1. Remember kNN is bad with high dimensions!

### Summary

Advantages
- Easy to build
 - Accelerates inference
Disadvantages
-  Curse of dimensionality makes it impractical for high values of $D$

Can we do better?
- Ball trees follow a different principle to space partitioning that makes them better for high dimensional spaces (not covered)
- We will exploit the idea behind KD trees to build decision trees

